        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Input Sertifikasi
                        </h1>

                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6">

                        <form role="form"  method="post" action="<?php echo site_url("ta_control/input_sertifikasi");?>" enctype="multipart/form-data">
                            <div class="form-group">
                                <select name="id">
								<option value="">Pilih Sertifikasi</option>
								<?php foreach($jenis as $ta){ ?>					
								<option value="<?php echo $ta->id_jenis_sertifikasi;?>"><?php echo	 $ta->nama_jenis_sertifikasi;?></option>
								<?php } ?>
								</select>                             
                            </div>
							 <div class="form-group">
                                <label>Tanggal Mulai Sertifikasi</label>
                                <a href="javascript:NewCssCal('mulai','yyyymmdd')">
                                <input type="text" class="form-control" name="mulai" id="mulai" required>                             
								
								</a>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Selesai Sertifikasi</label>
								<a href="javascript:NewCssCal('selesai','yyyymmdd')">
                                <input type="text" class="form-control" name="selesai" id="selesai" required>                             
								
								</a>
						   </div>
                            <div class="form-group">
                                <label>Alamat Sertifikasi</label>
                                <input class="form-control" name="alamat" required>                             
                            </div>
                            <button type="submit" class="btn btn-default">Input</button>
                            <button type="reset" class="btn btn-default">Reset</button>

                        </form>










                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>