        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        
                        <ol class="breadcrumb">
                           
                            <li class="active">
                                <i class="fa fa-dekstop"></i> <h2><?php echo $pen;
							?></h2>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6">

                        <form role="form"  method="post" action="<?php echo site_url("ta_control/ubah_sertifikasi");?>" enctype="multipart/form-data">
						<input type="hidden" name="id_jadwal" value="<?php echo $data->id_jadwal; ?>">
							<br></br>
							<div class="form-group">
                                <label>Tanggal Mulai Sertifikasi</label>
                                <a href="javascript:NewCssCal('mulai','yyyymmdd')">
                                <input type="text" class="form-control" name="mulai" id="mulai" value="<?php echo $data->tanggal_mulai_sertifikasi; ?>" required>                             
								
								</a>
                            </div>
							<div class="form-group">
                                <label>Tanggal Selesai Sertifikasi</label>
								<a href="javascript:NewCssCal('selesai','yyyymmdd')">
                                <input type="text" class="form-control" name="selesai" id="selesai" value="<?php echo $data->tanggal_selesai_sertifikasi; ?>" required>                             
								
								</a>
						   </div>
                            <div class="form-group">
                                <label>Alamat Sertifikasi</label>
                                <input class="form-control" name="alamat" value="<?php echo $data->alamat_sertifikasi; ?>">                             
                            </div>
                            <button type="submit" class="btn btn-default">Edit</button>
                            <button type="reset" class="btn btn-default">Reset</button>

                        </form>










                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>