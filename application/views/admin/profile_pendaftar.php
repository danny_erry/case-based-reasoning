 <div id="page-wrapper">

            <div class="container-fluid">
				<div class="row">
                    <div class="col-lg-12">
                        
                        <ol class="breadcrumb">
                           
                            <li class="active">
                                <i class="fa fa-dekstop"></i> <h2><?php echo $pen;
							?></h2>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- Page Heading -->

                <!-- /.row -->

                <div class="row">
				<div class="col-lg-6">

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tbody>
								<?php foreach($data as $baris){ ?>
                                    <tr>
                                        <td>Nama :</td>
                                        <td><?php echo $baris->nama;?></td>
                                    </tr>
									<tr>
                                        <td>Tempat Lahir :</td>
                                        <td><?php echo $baris->tempat_lahir;?></td>
                                    </tr>
									<tr>
                                        <td>Tanggal Lahir :</td>
                                        <td><?php echo tgl_indo($baris->tanggallahir);?></td>
                                    </tr>
									<tr>
                                        <td>Jenis Kelamin :</td>
                                        <td><?php echo $baris->jeniskelamin;?></td>
                                    </tr>
									<tr>
                                        <td>Kebangsaan :</td>
                                        <td><?php echo $baris->kebangsaan;?></td>
                                    </tr>
									<tr>
                                        <td>Alamat Rumah :</td>
                                        <td><?php echo $baris->alamatrumah;?></td>
                                    </tr>
									<tr>
                                        <td>Kode Pos Rumah :</td>
                                        <td><?php echo $baris->kodepos;?></td>
                                    </tr>
									<tr>
                                        <td>No. Telepon Rumah :</td>
                                        <td><?php echo $baris->noteleponrumah;?></td>
                                    </tr>
									<tr>
                                        <td>No. HP :</td>
                                        <td><?php echo $baris->nohape;?></td>
                                    </tr>
									<tr>
                                        <td>No. Telp. Kantor:</td>
                                        <td><?php echo $baris->noteleponkantor;?></td>
                                    </tr>
									<tr>
                                        <td>Pendidikan Terakir :</td>
                                        <td><?php echo $baris->pendidikanterakir;?></td>
                                    </tr>
									<tr>
                                        <td>Jurusan :</td>
                                        <td><?php echo $baris->jurusan;?></td>
                                    </tr>
									<tr>
                                        <td>Tahun Lulus :</td>
                                        <td><?php echo $baris->tahunlulus;?></td>
                                    </tr><tr>
                                        <td>Nama Lembaga :</td>
                                        <td><?php echo $baris->namalembaga;?></td>
                                    </tr><tr>
                                        <td>Jabatan :</td>
                                        <td><?php echo $baris->jabatan;?></td>
                                    </tr><tr>
                                        <td>Alamat Lembaga :</td>
                                        <td><?php echo $baris->alamatlembaga;?></td>
                                    </tr><tr>
                                        <td>Kode Pos Lembaga :</td>
                                        <td><?php echo $baris->kodeposlembaga;?></td>
                                    </tr><tr>
                                        <td>Sertifikasi Yang Diambil :</td>
                                        <td><?php echo $baris->nama_jenis_sertifikasi;?></td>
                                    </tr><tr>
                                        <td>No. Sertifikasi :</td>
                                        <td><?php echo $baris->no_sertifikasi;?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    
                    
                   
                </div>
                <!-- /.row -->

               
                <!-- /.row -->

                <div class="row">
                   
                    </div>
                  
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>