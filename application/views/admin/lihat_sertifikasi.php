 <div id="page-wrapper">

            <div class="container-fluid">
				<div class="row">
                    <div class="col-lg-12">
                        
                        <ol class="breadcrumb">
                           
                            <li class="active">
                                <i class="fa fa-dekstop"></i> <h2><?php echo $pen;
							?></h2>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- Page Heading -->

                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-88">
                        <div class="table-responsive">
						<form method="post" action="<?php echo site_url("ta_control/lihat_input_sertifikasi");?>">
						 <button type="submit" class="btn btn-default">Tambah jadwal</button>
						</form>
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Nama Sertifikasi</th>
                                        <th>Tanggal Mulai</th>
                                        <th>Tanggal Selesai</th>
                                        <th>Alamat</th>
										<th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php foreach($data as $baris){ ?>
                                    <tr>
                                        <td><?php echo $baris->nama_jenis_sertifikasi;?></td>
                                        <td><?php echo tgl_indo($baris->tanggal_mulai_sertifikasi);?></td>
                                        <td><?php echo tgl_indo($baris->tanggal_selesai_sertifikasi);?></td>
                                        <td><?php echo $baris->alamat_sertifikasi;?></td>
										 <td><a href="<?php echo site_url("ta_control/edit_sertifikasi/".$baris->id_jadwal); ?>"><img alt="edit" src="<?php echo base_url(); ?>assets/images/edit.png" />  </a>              
                &nbsp;<a href="<?php echo site_url("ta_control/hapus_sertifikasi/".$baris->id_jadwal); ?>" onclick="return confirm('Anda yakin akan menghapus data?')">
				<img alt="edit" src="<?php echo base_url(); ?>assets/images/delete.png" /></a></td>
                                    </tr>
										<?php } ?>
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                   
                </div>
                <!-- /.row -->

               
                <!-- /.row -->

                <div class="row">
                   
                    </div>
                  
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>