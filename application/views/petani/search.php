		<!--starts-blog-->
		<div class="blog">
			<div class="container">
				<div class="blog-main">
					<div class="col-md-8 blog-main-left">
						<h3>BLOG</h3>
						<div class="blg">
							<div class="col-md-4 blog-left">
								<a href="single.html"><img src="<?php echo base_url(); ?>asset/images/ape.jpg" alt=""></a>
							</div>
							<div class="col-md-8 blog-left">
								<h5>VULPUTATE VITAE DIGNISSIM</h5>
								<span>by <a href="#">user</a> on 09 july 2014   <a href="#">  comments&nbsp;(2)</a></span>
								<p>Suspendisse commodo tempor sagittis! In justo est, sollicitudin eu scelerisque pretium, placerat eget elit. Praesent faucibus rutrum odio at rhoncus. Pellentesque vitae tortor id neque fermentum pretium.</p>
								<div class="blog-btn">
									<a href="single.html">Read More</a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="blg">
							<div class="col-md-4 blog-left">
								<a href="single.html"><img src="<?php echo base_url(); ?>asset/images/vt.jpg" alt=""></a>
							</div>
							<div class="col-md-8 blog-left">
								<h5>VULPUTATE VITAE DIGNISSIM</h5>
								<span>by <a href="#">user</a> on 09 july 2014   <a href="#">  comments&nbsp;(2)</a></span>
								<p>Suspendisse commodo tempor sagittis! In justo est, sollicitudin eu scelerisque pretium, placerat eget elit. Praesent faucibus rutrum odio at rhoncus. Pellentesque vitae tortor id neque fermentum pretium.</p>
								<div class="blog-btn">
									<a href="single.html">Read More</a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="blg">
							<div class="col-md-4 blog-left">
								<a href="single.html"><img src="<?php echo base_url(); ?>asset/images/gp.jpg" alt=""></a>
							</div>
							<div class="col-md-8 blog-left">
								<h5>VULPUTATE VITAE DIGNISSIM</h5>
								<span>by <a href="#">user</a> on 09 july 2014   <a href="#">  comments&nbsp;(2)</a></span>
								<p>Suspendisse commodo tempor sagittis! In justo est, sollicitudin eu scelerisque pretium, placerat eget elit. Praesent faucibus rutrum odio at rhoncus. Pellentesque vitae tortor id neque fermentum pretium.</p>
								<div class="blog-btn">
									<a href="single.html">Read More</a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="col-md-4 blog-main-left">
						<h3>CATEGORIES</h3>
						<div class="ctgry">
						<ul>
							<li><a href="#">Nulla consectetur adipiscing metus</a></li>
							<li><a href="#">purus vitae varius sagittis</a></li>
							<li><a href="#">Epsum factorial non deposit quid</a></li>
							<li><a href="#">Donec eu elit in nisi placerat</a></li>
							<li><a href="#">Curabitur congue eros ac turpis</a></li>
						</ul>
						</div>
						<div class="archives">
							<h3>ARCHIVES</h3>
						<ul>
							<li><a href="#">November,2013</a></li>
							<li><a href="#">May,2013</a></li>
							<li><a href="#">April,2013</a></li>
							<li><a href="#">June,2013</a></li>
							<li><a href="#">August,2013</a></li>
							<li><a href="#">January,2013</a></li>
						</ul>
						</div>
						<div class="search">
							<h3>SEARCH</h3>
							<input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" />
							<input type="submit" value="Search" />
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="blog-bottom">
					<ul>
						<li><a href="#" class="active">PREV</a></li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">...</a></li>
						<li><a href="#">14</a></li>
						<li><a href="#">15</a></li>
						<li><a href="#" class="active">NEXT</a></li>
					</ul>
				</div>
			</div>
		</div>