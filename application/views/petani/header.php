<html>
<head>
<title>The Former Flat Responsive Agriculture bootstrap Website Template | Home :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">	
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<meta name="keywords" content="Bootstrap Responsive Templates, Iphone Compatible Templates, Smartphone Compatible Templates, Ipad Compatible Templates, Flat Responsive Templates"/>
<script src="<?php echo base_url(); ?>asset/js/jquery-1.11.0.min.js"></script>
<link href="<?php echo base_url(); ?>asset/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url(); ?>asset/css/style.css" rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
</head>
<body>
	<!--start-header-->
			<div id="home" class="header">
					<div class="top-header">
						<div class="container">
							<div class="logo">
								<a href="<?php echo site_url("pertanian/index"); ?>"><img src="<?php echo base_url(); ?>asset/images/logo.png" alt=""></a>  
							</div>
							<!--start-top-nav-->
							 <div class="top-nav">
							 <?php
							if($this->session->userdata('login'))
								{ ?>
								<ul>
								<?php $ambil = $this->session->userdata('login');
								foreach ($ambil as $stat) {
									$nama = $ambil['username'];
									}?>
									<li class="active"><a href="<?php echo site_url("ko_cko"); ?>"><span> </span><?php echo $nama; ?></a></li>
								</ul>
							<?php } else { ?>
								<ul>
									<li class="active"><a href="<?php echo site_url("pertanian/login"); ?>"><span> </span>Log in</a></li>
								</ul>
							<?php } ?>
							</div>
							<div class="clearfix"> </div>
						</div>
				</div>
			<!---pop-up-box---->
					  <script type="text/javascript" src="<?php echo base_url(); ?>asset/js/modernizr.custom.min.js"></script>    
					<link href="<?php echo base_url(); ?>asset/css/popup-box.css" rel="stylesheet" type="text/css" media="all"/>
					<script src="<?php echo base_url(); ?>asset/js/jquery.magnific-popup.js" type="text/javascript"></script>
					<!---//pop-up-box---->
				 <script>
						$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
																						
						});
				</script>					
		<!--End-header-->
		<div class="navgation">
					<div class="menu">
                         <a class="toggleMenu" href="#"><img src="<?php echo base_url(); ?>asset/images/menu-icon.png" alt="" /> </a>
							<ul class="nav" id="nav">
							<li><a href="<?php echo site_url("pertanian/index"); ?>">Home</a></li>
							<li><a href="<?php echo site_url("pertanian/ilmu"); ?>">Ilmu Pertanian</a></li>
							<li><a href="<?php echo site_url("pertanian/penyakit"); ?>">Penyakit</a></li>
							
							<li><a href="contact.html">Contact</a></li>
							</ul>
                            <!----start-top-nav-script---->
		                     <script type="text/javascript" src="<?php echo base_url(); ?>asset/js/responsive-nav.js"></script>
							<script type="text/javascript">
							jQuery(document).ready(function($) {
								$(".scroll").click(function(event){		
									event.preventDefault();
									$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
								});
							});
							</script>
							<!----//End-top-nav-script---->
					</div>
					<div class="search2">
					  <form method="post" action="<?php echo site_url("pertanian/cari");?>">
						 <input type="text" name="cari" />
						 <input type="submit" value="">
					  </form>
					</div>
					<div class="clearfix"> </div>
		</div>
		</div>
		