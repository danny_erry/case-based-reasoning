	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo site_url("siswa/home"); ?>"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active"><?php echo $alamat ?></li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	<?php echo validation_errors() ?>
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<article>
				<form role="form"  method="post" action="<?php echo site_url("siswa/input_pendaftar");?>" enctype="multipart/form-data">
					<h1>Data Pribadi</h1>
					<div class="form-group">
                        <label>Nama</label>
						<input type="hidden" class="form-control" name="id" value="<?php echo $id;?>">
                        <input class="form-control" name="nama" placeholder="Nama" required>                             
                    </div>
					<div class="form-group">
                        <label>Tempat Lahir</label>
                        <input class="form-control" name="tempatlahir" placeholder="Tempat Lahir" required>                             
                    </div>
					<div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input class="form-control" name="tanggallahir" placeholder="17-08-1945" required>                             
                    </div>
					<div class="form-group">
                        <label>Jenis Kelamin</label>
                        <input type="radio" name="jeniskelamin" value="Pria" /> Pria &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" name="jeniskelamin" value="Wanita" /> Wanita
                    </div>
					<div class="form-group">
                        <label>Kebangsaan</label>
                        <select name='kebangsaan'>
				<option value="WNI">WNI</option>
				<option value="WNA">WNA</option>
				</select>
                    </div>
					<div class="form-group">
                        <label>Alamat Rumah</label>
                        <input class="form-control" name="alamatrumah" placeholder="Alamat Rumah" required>                             
                    </div>
					<div class="form-group">
                        <label>Kode Pos</label>
                        <input class="form-control" name="kodepos" placeholder="Kode Pos" required>                             
                    </div>
					<div class="form-group">
                        <label>No Telepon Rumah</label>
                        <input class="form-control" name="teleponrumah" placeholder="Kosongkan Bila Tidak Ada">                             
                    </div>
					<div class="form-group">
                        <label>No HP</label>
                        <input class="form-control" name="hp" placeholder="Nomor HP" required>                             
                    </div>
					<div class="form-group">
                        <label>No Telepon Kantor</label>
                        <input class="form-control" name="teleponkantor" placeholder="Kosongkan Bila Belum Bekerja">                             
                    </div>
					<h1>Data Pendidikan</h1>
					<div class="form-group">
                        <label>Pendidikan Terakir</label>
                        <input class="form-control" name="pendidikanterakir" placeholder="Pendidikan Formal" required>                             
                    </div>
					<div class="form-group">
                        <label>Jurusan/Program</label>
                        <input class="form-control" name="jurusan" placeholder="Jurusan/Program" required>                             
                    </div>
					<div class="form-group">
                        <label>Tahun Lulus</label>
                        <input class="form-control" name="lulus" placeholder="Tahun Lulus" required>                             
                    </div>
					<h1>Data Pekerjaan</h1>
					<div class="form-group">
                        <label>Nama Lembaga/Perusaan</label>
                        <input class="form-control" name="namalembaga" placeholder="Nama Lembaga">                             
                    </div>
					<div class="form-group">
                        <label>Jabatan</label>
                        <input class="form-control" name="jabatan" placeholder="Jabatan">                             
                    </div>
					<div class="form-group">
                        <label>Alamat Lembaga/Perusaan</label>
                        <input class="form-control" name="alamatlembaga" placeholder="Alamat Lembaga">                             
                    </div>
					<div class="form-group">
                        <label>Kode Pos</label>
                        <input class="form-control" name="kodeposlembaga" placeholder="Kode Pos Alamat Lembaga">                             
                    </div>
					<button type="submit" class="btn btn-default" onclick="return confirm('Apakah data Tersebut Benar ?')">Daftar</button>
                    <button type="reset" class="btn btn-default">Reset</button>
				</form>
				</article>
				
			</div>
			<div class="col-lg-4">
				<aside class="right-sidebar">
				<div class="widget">
				<h5 class="widgetheading">Menu</h5>
					<ul class="cat">
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/index"); ?>">Home</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/lihat_galery"); ?>">Galery</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/lihat_alumni"); ?>">Data Alumni</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/contact"); ?>">Contact</a></li>
						
					</ul>
			</div>
				
				</aside>
			</div>
		</div>
	</div>
	</section>