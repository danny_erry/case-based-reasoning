	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo site_url("siswa/home"); ?>"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active"><?php echo $alamat ?></li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<article>
					<h1>Daftar Sertifikasi</h1>
<table>
<tr>
<th><h6>Nama Sertifikasi</h6></th>
<th><h6>Tanggal Mulai</h6></th>
<th><h6>Tanggal Selesai</h6></th>
<th><h6>Alamat Sertifikasi</h6></th>
<th><h6>Daftar Sekarang</h6></th>
</tr>
<?php foreach($param as $baris){ ?>
<tr>
	<td><?php echo $baris->nama_jenis_sertifikasi;?></td>
    <td><?php echo tgl_indo($baris->tanggal_mulai_sertifikasi);?></td>
    <td><?php echo tgl_indo($baris->tanggal_selesai_sertifikasi);?></td>
    <td><?php echo $baris->alamat_sertifikasi;?></td>
	<td><a href="<?php echo site_url("siswa/input_pendaftaran/".$baris->id_sertifikasi); ?>">Daftar</a></td>
</tr>
<?php } ?>
</table>
				</article>
				
			</div>
			<div class="col-lg-4">
				<aside class="right-sidebar">
				<div class="widget">
				<h5 class="widgetheading">Menu</h5>
					<ul class="cat">
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/index"); ?>">Home</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/lihat_galery"); ?>">Galery</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/lihat_alumni"); ?>">Data Alumni</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/contact"); ?>">Contact</a></li>
						
					</ul>
			</div>
				
				</aside>
			</div>
		</div>
	</div>
	</section>