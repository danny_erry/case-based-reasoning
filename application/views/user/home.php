
	<!-- end header -->
	<section id="featured">
	<!-- start slider -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
	<!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
			<?php foreach($data as $baris){ ?>
              <li>
                <img src="<?php echo base_url(); ?>upload_image/<?php echo $baris->url_slider;?>" alt="" width="1024px" height="360px" />
                <div class="flex-caption">
                    <h3><?php echo $baris->nama_slider;?></h3> 
					<p><?php echo $baris->keterangan_slider;?></p> 
				
                </div>
              </li>
			  	<?php } ?>
            </ul>
        </div>
	<!-- end slider -->
			</div>
		</div>
	</div>	
	
	

	</section>
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
			
				<img src="<?php echo base_url(); ?>assets/images/telkom.jpg" alt="" class="align-left" />
				
			</div>
			<?php foreach($isi as $isi){ ?>
			<div class="col-lg-6">
				<h4><?php echo $isi->judul_home;?></h4>
				<p>
				
              <?php echo $isi->isi_home;?>
			  	<?php } ?>
				</p>
				
			</div>
			<div class="widget">
				<h5 class="widgetheading">Menu</h5>
					<ul class="cat">
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/index"); ?>">Home</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/lihat_galery"); ?>">Galery</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/lihat_alumni"); ?>">Data Alumni</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/contact"); ?>">Contact</a></li>
						
					</ul>
			</div>
		</div>
		<!-- divider -->
		<div class="row">
			<div class="col-lg-12">
				<div class="solidline">
				</div>
			</div>
		</div>
		<!-- end divider -->
		<!-- Descriptions -->

		<!-- divider -->
		<div class="row">
			<div class="col-lg-12">
				<div class="solidline">
				</div>
			</div>
		</div>
		<!-- end divider -->
		<!-- Lists -->
		
		<!-- divider -->
		<div class="row">
			<div class="col-lg-12">
				<div class="solidline">
				</div>
			</div>
		</div>
		<!-- end divider -->
	
		<!-- divider -->
		<div class="row">
			<div class="col-lg-12">
				<div class="solidline">
				</div>
			</div>
		</div>
		<!-- end divider -->

	</div>
	</section>