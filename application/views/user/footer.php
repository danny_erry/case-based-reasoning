	<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
			<img src="<?php echo base_url(); ?>assets/modema/img/facebook.png" height="10%" width="10%"><a href="https://id-id.facebook.com/SMKTelkomSPMalang" target="blank"> Facebook </a>
			</div>
			<div class="col-lg-3">
			<img src="<?php echo base_url(); ?>assets/modema/img/twitter.jpg" height="10%" width="10%"><a href="https://twitter.com/smktelkommalang" target="blank"> @smktelkommalang </a>
			</div>
			<div class="col-lg-3">
			<img src="<?php echo base_url(); ?>assets/modema/img/email.png" height="10%" width="10%"> info@smktelkom-mlg.sch.id
			</div>
			<div class="col-lg-3">
			<img src="<?php echo base_url(); ?>assets/modema/img/web.jpg" height="10%" width="10%"><a href="http://www.smktelkom-mlg.sch.id/" target="blank"> Website </a>
			</div>
		</div>
	</div>

	</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>assets/modema/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/modema/js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url(); ?>assets/modema/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/modema/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url(); ?>assets/modema/js/jquery.fancybox-media.js"></script>
<script src="<?php echo base_url(); ?>assets/modema/js/google-code-prettify/prettify.js"></script>
<script src="<?php echo base_url(); ?>assets/modema/js/portfolio/jquery.quicksand.js"></script>
<script src="<?php echo base_url(); ?>assets/modema/js/portfolio/setting.js"></script>
<script src="<?php echo base_url(); ?>assets/modema/js/jquery.flexslider.js"></script>
<script src="<?php echo base_url(); ?>assets/modema/js/animate.js"></script>
<script src="<?php echo base_url(); ?>assets/modema/js/custom.js"></script>
</body>
</html>