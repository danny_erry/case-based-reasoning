<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo site_url("siswa/home"); ?>"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active"><?php echo $alamat ?></li>
				</ul>
			</div>
		</div>
	</div>
	</section>

<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="portfolio-categ filter">
					<li><a href="<?php echo site_url("siswa/lihat_galery"); ?>">All</a></li>
					<li><a href="<?php echo site_url("siswa/lihat_ujian"); ?>" title="">Ujian</a></li>
					<li><a href="<?php echo site_url("siswa/lihat_bootcamp"); ?>" title="">Boot Camp</a></li>
					<li><a href="<?php echo site_url("siswa/lihat_umum"); ?>" title="">Umum</a></li>
					<li><form>
					<select name="tahun" onChange='this.form.submit()'>
					<option value="">Pilih Tahun</option>
					<?php foreach($tah as $ta){ ?>
					
					<option value="<?php echo	 $ta->tahun_foto;?>"><?php echo	 $ta->tahun_foto;?></option>
					<?php } ?>
</select>
					</form></li>
				</ul>
				<div class="clearfix">
				</div>
				<div class="row">
					<section id="projects">
					<?php foreach($data as $baris){ ?>	
					<ul id="thumbs" class="portfolio">
						<li class="item-thumbs col-lg-3 design">
						<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Foto <?php echo $baris->kategori_foto;?>-<?php echo $baris->tahun_foto;?>" href="<?php echo base_url(); ?>/upload_image/<?php echo $baris->url_foto;?>">
						<span class="overlay-img"></span>
						<span class="overlay-img-thumb font-icon-plus"></span>
						</a>
						<img src="<?php echo base_url(); ?>/upload_image/<?php echo $baris->url_foto;?>" height="225px" width="200px" alt="<?php echo $baris->judul_foto;?>">
						</li>
						<?php } ?>
						<!-- End Item Project -->
					</ul>
					</section>
				</div>
			</div>
		</div>
	</div>
	</section>