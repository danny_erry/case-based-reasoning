	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="<?php echo site_url("siswa/home"); ?>"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active"><?php echo $alamat ?></li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<article>
				<h1>Data Alumni</h1>
				<ul class="portfolio-categ filter">
					<li><form>
					Cari Tahun Lulus <select name="tahun" onChange='this.form.submit()'>
					<option value="">Pilih Tahun</option>
					<?php foreach($tah as $ta){ ?>					
					<option value="<?php echo	 $ta->tahun_lulus_ser;?>"><?php echo	 $ta->tahun_lulus_ser;?></option>
					<?php } ?>
					</select>
					</form></li>
					<li>
					<form>
					<input type="text" name="cari" placeholder="Cari Nama">&nbsp;<input type="submit" value="Cari"></form>
					</li>
				</ul>
<table>
<tr>
<th><h6>Nama</h6></th>
<th><h6>No Sertifikasi</h6></th>
<th><h6>Tahun Lulus</h6></th>
</tr>
<?php foreach($data as $baris){ ?>
<tr>
	<td><?php echo $baris->nama;?></td>
    <td><?php echo $baris->no_sertifikasi;?></td>
	<td><?php echo $baris->tahun_lulus_ser;?></td>
</tr>
<?php } ?>
</table>
				</article>
				
			</div>
			<div class="col-lg-4">
				<aside class="right-sidebar">
				<div class="widget">
				<h5 class="widgetheading">Menu</h5>
					<ul class="cat">
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/index"); ?>">Home</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/lihat_galery"); ?>">Galery</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/lihat_alumni"); ?>">Data Alumni</a></li>
						<li><i class="icon-angle-right"></i><a href="<?php echo site_url("siswa/contact"); ?>">Contact</a></li>
						
					</ul>
			</div>
				
				</aside>
			</div>
		</div>
	</div>
	</section>