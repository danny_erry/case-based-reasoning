<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_skripsi extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
	function login($username, $password)
	{
		$this -> db -> select('*');
		$this -> db -> from('stake_holder');
		$this -> db -> where('user_name', $username);
		$this -> db -> where('password', $password);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		if($query -> num_rows() == 1)
			{
     
				$result = $query->result();    
				return $result;
			}
		else
			{
				return false;
			}
	}
	
	function data_ilmu($id)
	{
	$this -> db -> select('*');
	$this -> db -> from('data_ilmu_pertanian');
	$this -> db -> where('id_Stake_holder', $id);
	$params = $this -> db -> get();
	return $params->result();
	}
	
	public function masuk_ilmu($data)
	{
		$this->db->insert("data_ilmu_pertanian", $data);
	}
  
}