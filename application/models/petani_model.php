<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Petani_model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function ambil_ilmu($num, $offset)
	{
	$this->db->select('id_data_ilmu_pertanian,judul_data_pertanian,isi_data_pertanian,last_update_ilmu,nama_pejabat,url_gambar_ilmu');
	$this->db->from('data_ilmu_pertanian');
	$this->db->join("stake_holder", "data_ilmu_pertanian.id_stake_holder=stake_holder.id_stake_holder",'inner');
	$this->db->limit($num,$offset);
	$this->db->order_by('id_data_ilmu_pertanian', 'ASC');
	$data=$this->db->get();
	return $data->result();
	}
	
	public function lihat_ilmu($id)
	{
	$this->db->select('*');
	$this->db->from('data_ilmu_pertanian');
	$this->db->where('id_data_ilmu_pertanian',$id);
	$data=$this->db->get();
	return $data->result();
	}
	
	public function lihat_penyakit($id)
	{
	$this->db->select('*');
	$this->db->from('data_penyakit');
	$this->db->where('id_penyakit',$id);
	$data=$this->db->get();
	return $data->result();
	}
	
	public function penyakit()
	{
	$this->db->select('*');
	$this->db->from('gejala');
	$data=$this->db->get();
	return $data->result();
	}
	
	public function gej()
	{
	$this->db->select('*');
	$this->db->from('data_penyakit');
	$this->db->where('id_jenis_tanaman','1');
	$data=$this->db->get();
	return $data->result();
	}
	
	public function solusi($id)
	{
	$this->db->select('*');
	$this->db->from('data_penyakit');
	$this->db->where('id_penyakit',$id);
	$data=$this->db->get();
	return $data->result();
	}
	
	public function identifikasi($id)
	{
		$id=$this->input->get($id);
		if($id==''){	
		}else {
		
		$data['nama_gejala']=$id;
		return $this->db->insert("gejala",$data);
		}
	}
	
	public function cari_data()
	{
		$cari=$this->param = $this->input->post('cari'); 
		$this->db->select('*');
		$this->db->from('data_ilmu_pertanian');
		$this->db->like('judul_data_pertanian',$cari);
		$this->db->or_like('isi_data_pertanian',$cari);
		$params=$this->db->get();
		return $params->result();    
	}
	
	public function cari_penyakit()
	{
		$cari=$this->param = $this->input->post('cari'); 
		$this->db->select('*');
		$this->db->from('data_penyakit');
		$this->db->like('nama_penyakit',$cari);
		$this->db->or_like('gejala_penyakit',$cari);
		$query=$this->db->get();
		return $query->result();    
	}
}