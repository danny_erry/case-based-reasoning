<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
	function input_mahasiswa()
	{
		$data['nrp']	= $this->input->post("nrp");
    }
	
	function lihat_mahasiswa()
	{
		$this->db->select('*');
		$this->db->from('mahasiswa');
		$params=$this->db->get();
		return $params->result();
	}
	
	function masuk_mahasiswa()
	{
		$data['nrp']=$this->input->post("nrp");
		$data['nama']=$this->input->post("nama");
		$data['alamat']=$this->input->post("alamat");
		$data['jenis_kelamin']=$this->input->post("jenis_kelamin");
		$data['no_telpon']=$this->input->post("telepon");
		return $this->db->insert("mahasiswa",$data);
	}
	
	function get_nrp($id)
	{
		$params=$this->db->get_where('mahasiswa',array('nrp'=>$id));
		return $params->row();
	}
	
	function edit_mahasiswa($id)
	{
		$id=$this->input->post('nrp');
		$ubah=array(
				'nama'=>$this->input->post('nama'),
				'alamat'=>$this->input->post('alamat'),
				'no_telpon'=>$this->input->post('telepon'),
				'jenis_kelamin'=>$this->input->post('jenis_kelamin')
				);
				$this->db->where('nrp',$id);
				$this->db->update('mahasiswa',$ubah);						
	}
	
	function delete_mahasiswa($id)
	{
	$this->db->where('nrp',$id);
	$this->db->delete('mahasiswa');
	}
}