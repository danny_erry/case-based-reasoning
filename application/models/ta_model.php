<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ta_model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
		
	function lihat_sertifikasi()
	{
		$this->db->select('id_jadwal,nama_jenis_sertifikasi,tanggal_mulai_sertifikasi,tanggal_selesai_sertifikasi,alamat_sertifikasi');
		$this->db->from('sertifikasi');
		$this->db->join("jenis_sertifikasi", "jenis_sertifikasi.id_jenis_sertifikasi=sertifikasi.id_Sertifikasi",'inner');
		$this->db->where('sh_sertifikasi','1');
		$this->db->order_by('nama_jenis_sertifikasi');
		$this->db->order_by('tanggal_mulai_sertifikasi');
		$params=$this->db->get();
		return $params->result();
	}
	
	function input_sertifikasi()
	{
		$data['id_sertifikasi']=$this->input->post("id");
		$data['tanggal_mulai_sertifikasi']=$this->input->post("mulai");
		$data['tanggal_selesai_sertifikasi']=$this->input->post("selesai");
		$data['alamat_sertifikasi']=$this->input->post("alamat");
		$data['sh_sertifikasi']='1';
		return $this->db->insert("sertifikasi",$data);
	}
	
	function get_sertifikasi($id)
	{
		$params=$this->db->get_where('sertifikasi',array('id_jadwal'=>$id));
		return $params->row();
	}
	
	function edit_sertifikasi($id)
	{
		$id=$this->input->post('id_jadwal');
		$ubah=array(
				
				'tanggal_mulai_sertifikasi'=>$this->input->post('mulai'),
				'tanggal_selesai_sertifikasi'=>$this->input->post('selesai'),
				'alamat_sertifikasi'=>$this->input->post('alamat')
				);
		$this->db->where('id_jadwal',$id);
		$this->db->update('sertifikasi',$ubah);						
	}
	
	function delete_sertifikasi($id)
	{
	$ubah=array(
				'sh_sertifikasi'=>'0'
				);
		$this->db->where('id_jadwal',$id);
		$this->db->update('sertifikasi',$ubah);
	}
	
 
	public function upload_gambar($data)
	{
		$this->db->insert("foto", $data);
	}

	public function upload_slider($data)
	{
		$this->db->insert("slider", $data);
	}
	
	public function lihat_slider()
	{
		$this->db->select('*');
		$this->db->from('slider');
		$this->db->where('sh_slider','1');
		$params=$this->db->get();
		return $params->result();
	}
	
	public function lihat_ujian()
	{
		$id=$this->input->get('tahun');
		if ($id==''){
		$x=date('Y');
		}else{
		$x=$id;
		}
		$this->db->select('*');
		$this->db->from('foto');
		$this->db->where('kategori_foto','ujian');
		$this->db->where('sh_foto','1');
		$this->db->where('tahun_foto',$x);
		$params=$this->db->get();
		return $params->result();
	}
	
	public function lihat_bootcamp()
	{
		$id=$this->input->get('tahun');
		if ($id==''){
		$x=date('Y');
		}else{
		$x=$id;
		}
		$this->db->select('*');
		$this->db->from('foto');
		$this->db->where('kategori_foto','bootcamp');
		$this->db->where('sh_foto','1');
		$this->db->where('tahun_foto',$x);
		$params=$this->db->get();
		return $params->result();
	}
	
	public function lihat_umum()
	{
		$id=$this->input->get('tahun');
		if ($id==''){
		$x=date('Y');
		}else{
		$x=$id;
		}
		$this->db->select('*');
		$this->db->from('foto');
		$this->db->where('kategori_foto','umum');
		$this->db->where('sh_foto','1');
		$this->db->where('tahun_foto',$x);
		$params=$this->db->get();
		return $params->result();
	}
	
	function get_foto($id)
	{
		$params=$this->db->get_where('foto',array('id_foto'=>$id));
		return $params->row();
	}
	
	function get_slider($id)
	{
		$params=$this->db->get_where('slider',array('id_slider'=>$id));
		return $params->row();
	}
	
	function edit_foto($id)
	{
		$id=$this->input->post('id_foto');
		$ubah=array(
				'judul_foto'=>$this->input->post('judul')
				);
		$this->db->where('id_foto',$id);
		$this->db->update('foto',$ubah);						
	}
	
	function edit_slider($id)
	{
		$id=$this->input->post('id_slider');
		$ubah=array(
				'nama_slider'=>$this->input->post('judul'),
				'keterangan_slider'=>$this->input->post('keterangan')
				);
		$this->db->where('id_slider',$id);
		$this->db->update('slider',$ubah);						
	}
	
	function delete_foto($id)
	{
	$ubah=array(
				'sh_foto'=>'0'
				);
		$this->db->where('id_foto',$id);
		$this->db->update('foto',$ubah);
	}
	
	function delete_slider($id)
	{
	$ubah=array(
				'sh_slider'=>'0'
				);
		$this->db->where('id_slider',$id);
		$this->db->update('slider',$ubah);
	}
	
	
	function lihat_pendaftar()
	{
		$this->db->select('id_peserta,nama,alamatrumah,nohape,nama_jenis_sertifikasi,id_sertifikasi');
		$this->db->from('peserta');
		$this->db->join("jenis_sertifikasi", "jenis_sertifikasi.id_jenis_sertifikasi=peserta.id_Sertifikasi",'inner');
		$this->db->where('no_sertifikasi','');
		$params=$this->db->get();
		if($params -> num_rows() < 1)
		{ 
			return false;
		
		}
		else
		{
		return $params->result();
		}
	}
	
	function lihat_pendaftar_pisah($id)
	{
		$this->db->select('id_peserta,nama,alamatrumah,nohape,nama_jenis_sertifikasi,id_sertifikasi');
		$this->db->from('peserta');
		$this->db->join("jenis_sertifikasi", "jenis_sertifikasi.id_jenis_sertifikasi=peserta.id_Sertifikasi",'inner');
		$this->db->where('no_sertifikasi','');
		$this->db->where('id_jenis_sertifikasi',$id);
		$params=$this->db->get();
		if($params -> num_rows() < 1)
		{ 
			return false;
		
		}
		else
		{
		return $params->result();
		}
	}
	
	function lihat_semua_pendaftar()
	{
		$this->db->select('id_peserta,nama,alamatrumah,nohape,nama_jenis_sertifikasi,id_sertifikasi');
		$this->db->from('peserta');
		$this->db->join("jenis_sertifikasi", "jenis_sertifikasi.id_jenis_sertifikasi=peserta.id_Sertifikasi",'inner');
		$this->db->where_not_in('no_sertifikasi','');
		$params=$this->db->get();
		return $params->result();
	}
	
	function lihat_semua_pendaftar_pisah($id)
	{
		$this->db->select('id_peserta,nama,alamatrumah,nohape,nama_jenis_sertifikasi,id_sertifikasi');
		$this->db->from('peserta');
		$this->db->join("jenis_sertifikasi", "jenis_sertifikasi.id_jenis_sertifikasi=peserta.id_Sertifikasi",'inner');
		$this->db->where_not_in('no_sertifikasi','');
		$this->db->where('id_jenis_sertifikasi',$id);
		$params=$this->db->get();
		return $params->result();
	}
	
	function profile_pendaftar($id)
	{
		$this->db->select('id_peserta,nama,tempat_lahir,tanggallahir,jeniskelamin,kebangsaan,alamatrumah,kodepos,noteleponrumah
		,nohape,noteleponkantor,pendidikanterakir,jurusan,tahunlulus,namalembaga,jabatan,alamatlembaga,kodeposlembaga,nama_jenis_sertifikasi,no_sertifikasi');
		$this->db->from('peserta');
		$this->db->join("jenis_sertifikasi", "jenis_sertifikasi.id_jenis_sertifikasi=peserta.id_Sertifikasi",'inner');
		$this->db->where('id_peserta',$id);
		$params=$this->db->get();
		return $params->result();
	}
	
	function lihat_pesan()
	{
		$this->db->select('*');
		$this->db->from('pesan');
		$this->db->where('sh_pesan','1');
		$params=$this->db->get();
		return $params->result();
	}
	
	
	function login($username, $password)
  {
   $this -> db -> select('username, password,nama');
   $this -> db -> from('login');
   $this -> db -> where('username', $username);
   $this -> db -> where('password', $password);
   $this -> db -> limit(1);
    
   $query = $this -> db -> get();
    
   if($query -> num_rows() == 1)
   {
     
    $result = $query->result();
     
    return $result;
   }
   else
   {
    return false;
   }
  }
  
  function no_sertifikasi()
	{
	$id=$this->input->post('id');
	$ubah=array(
				'no_sertifikasi'=>$this->input->post('noser'),
				'tahun_lulus_ser'=>$this->input->post('tahunlulus')
				);
		$this->db->where('id_peserta',$id);
		$this->db->update('peserta',$ubah);
	}
	
	function baca_pesan($id)
	{
	$ubah=array(
				'sh_pesan'=>'0'
				);
		$this->db->where('id_pesan',$id);
		$this->db->update('pesan',$ubah);
	}
	
	function lihat_admin()
	{
		$this->db->select('*');
		$this->db->from('login');
		$params=$this->db->get();
		return $params->result();
	}
	
	function ubah_pas($id)
	{
		$this->db->select('*');
		$this->db->from('login');
		$this->db->where('username',$id);
		$params=$this->db->get();
		return $params->result();
	}
	
	public function ganti_pas($data,$user)
	{
		$this->db->where('username',$user);
		$this->db->update('login',$data);
	}
	
	public function lihat_tahun()
	{
		$this->db->distinct('tahun_foto');
		$this->db->select('tahun_foto');
		$this->db->from('foto');
		$this->db->where('sh_foto','1');
		$this->db->order_by('tahun_foto', 'desc');
		$params=$this->db->get();
		return $params->result();
	}
	public function lihat_tahun_lulus()
	{
		$this->db->distinct('tahun_lulus_ser');
		$this->db->select('tahun_lulus_ser');
		$this->db->from('peserta');
		$this->db->where_not_in('tahun_lulus_ser','');
		$this->db->order_by('tahun_lulus_ser', 'desc');
		$params=$this->db->get();
		return $params->result();
	}
	
	public function lihat_jenis_sertifikasi()
	{
		$this->db->select('*');
		$this->db->from('jenis_Sertifikasi');
		$this->db->where('sh_jenis_sertifikasi','1');
		$params=$this->db->get();
		return $params->result();
	}
	
	function masuk_alumni($id)
	{	
			
			$id=$this->input->get("$id");
			if($id==''){	
			}else {
			$data['id_alumni']=$id;
			return $this->db->insert("alumni",$data);
			}
		
		
	}
	
	function input_jenis_sertifikasi()
	{
		$data['nama_jenis_sertifikasi']=$this->input->post("nama");
		$data['sh_jenis_sertifikasi']='1';
		return $this->db->insert("jenis_sertifikasi",$data);
	}
	
	function get_jenis_sertifikasi($id)
	{
		$params=$this->db->get_where('jenis_sertifikasi',array('id_jenis_sertifikasi'=>$id));
		return $params->row();
	}
	
	function edit_jenis_sertifikasi()
	{
		$id=$this->input->post('id_jenis_Sertifikasi');
		$ubah=array(
				'nama_jenis_sertifikasi'=>$this->input->post('nama')
				);
		$this->db->where('id_jenis_Sertifikasi',$id);
		$this->db->update('jenis_Sertifikasi',$ubah);						
	}
	
	function delete_jenis_sertifikasi($id)
	{
	$ubah=array(
				'sh_jenis_sertifikasi'=>'0'
				);
		$this->db->where('id_jenis_Sertifikasi',$id);
		$this->db->update('jenis_sertifikasi',$ubah);
	}
	
	public function lihat_home()
	{
		$this->db->select('*');
		$this->db->from('home');
		$params=$this->db->get();
		return $params->result();
	}
	
	function get_home($id)
	{
		$params=$this->db->get_where('home',array('id_home'=>$id));
		return $params->row();
	}
	
	function ubah_home()
	{
		$id=$this->input->post('id');
		$ubah=array(
				'isi_home'=>$this->input->post('isi'),
				'judul_home'=>$this->input->post('judul')
				);
		$this->db->where('id_home',$id);
		$this->db->update('home',$ubah);						
	}

}