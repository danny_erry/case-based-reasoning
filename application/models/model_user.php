<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_user extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function lihat_slider()
	{
		$this->db->select('*');
		$this->db->from('slider');
		$this->db->where('sh_slider','1');
		$params=$this->db->get();
		return $params->result();
	}

	public function lihat_galery()
	{
		$id=$this->input->get('tahun');
		if ($id==''){
		$x=date('Y');
		}else{
		$x=$id;
		}
		$this->db->select('*');
		$this->db->from('foto');
		$this->db->where('sh_foto','1');
		$this->db->where('tahun_foto',$x);
		$params=$this->db->get();
		return $params->result();
	}
	
	public function lihat_tahun()
	{
		$this->db->distinct();
		$this->db->select('tahun_foto');
		$this->db->from('foto');
		$this->db->where('sh_foto','1');
		$this->db->order_by('tahun_foto', 'desc');
		$params=$this->db->get();
		return $params->result();
	}
	
	public function lihat_tahun_lulus()
	{
		$this->db->distinct();
		$this->db->select('tahun_lulus_ser');
		$this->db->from('peserta');
		$this->db->where_not_in('tahun_lulus_Ser','');
		$this->db->order_by('tahunlulus', 'desc');
		$params=$this->db->get();
		return $params->result();
	}

	public function lihat_ujian()
	{
		$id=$this->input->get('tahun');
		if ($id==''){
		$x=date('Y');
		}else{
		$x=$id;
		}
		$this->db->select('*');
		$this->db->from('foto');
		$this->db->where('kategori_foto','ujian');
		$this->db->where('sh_foto','1');
		$this->db->where('tahun_foto',$x);
		$params=$this->db->get();
		return $params->result();
	}
	
	public function lihat_bootcamp()
	{
		$id=$this->input->get('tahun');
		if ($id==''){
		$x=date('Y');
		}else{
		$x=$id;
		}
		$this->db->select('*');
		$this->db->from('foto');
		$this->db->where('kategori_foto','bootcamp');
		$this->db->where('sh_foto','1');
		$this->db->where('tahun_foto',$x);
		$params=$this->db->get();
		return $params->result();
	}
	
	public function lihat_umum()
	{
		$id=$this->input->get('tahun');
		if ($id==''){
		$x=date('Y');
		}else{
		$x=$id;
		}
		$this->db->select('*');
		$this->db->from('foto');
		$this->db->where('kategori_foto','umum');
		$this->db->where('sh_foto','1');
		$this->db->where('tahun_foto',$x);
		$params=$this->db->get();
		return $params->result();
	}
	
	public function lihat_sertifikasi($id)
	{
		$id_sertifikasi=$id;
		$this->db->select('id_jadwal,id_sertifikasi,nama_jenis_sertifikasi,tanggal_mulai_sertifikasi,tanggal_selesai_sertifikasi,alamat_sertifikasi');
		$this->db->from('sertifikasi');
		$this->db->join("jenis_sertifikasi", "jenis_sertifikasi.id_jenis_sertifikasi=sertifikasi.id_Sertifikasi",'inner');
		$this->db->where('id_sertifikasi',$id_sertifikasi);
		$this->db->where('sh_sertifikasi','1');
		$this->db->order_by('tanggal_mulai_sertifikasi');
		$params=$this->db->get();
		return $params->result();
	}
	
	function input_pendaftar()
	{
		$data['id_sertifikasi']=$this->input->post("id");
		$data['nama']=$this->input->post("nama");

		$data['tempat_lahir']=$this->input->post("tempatlahir");
		
		$tanggal=$this->input->post("tanggallahir");
		$pecah = explode("-",$tanggal);
		$tahun = $pecah[2];
        $bulan = $pecah[1];
        $tgl = $pecah[0];
		$tl=$tahun.'-'.$bulan.'-'.$tgl;
		$data['tanggallahir']=$tl;
		$data['jeniskelamin']=$this->input->post("jeniskelamin");
		$data['kebangsaan']=$this->input->post("kebangsaan");
		$data['alamatrumah']=$this->input->post("alamatrumah");
		$data['kodepos']=$this->input->post("kodepos");
		$data['noteleponrumah']=$this->input->post("teleponrumah");
		$data['nohape']=$this->input->post("hp");
		$data['noteleponkantor']=$this->input->post("teleponkantor");
		$data['pendidikanterakir']=$this->input->post("pendidikanterakir");
		$data['jurusan']=$this->input->post("jurusan");
		$data['tahunlulus']=$this->input->post("lulus");
		$data['namalembaga']=$this->input->post("namalembaga");
		$data['jabatan']=$this->input->post("jabatan");
		$data['alamatlembaga']=$this->input->post("alamatlembaga");
		$data['kodeposlembaga']=$this->input->post("kodeposlembaga");
		return $this->db->insert("peserta",$data);
	}
	
	function input_pesan()
	{
		$data['nama_pengirim']=$this->input->post("name");
		$data['email_pengirim']=$this->input->post("email");
		$data['subject_pengirim']=$this->input->post("subject");
		$data['isi_pesan']=$this->input->post("message");
		$data['sh_pesan']='1';
		$data['tanggal']=date('Y-m-d');
		return $this->db->insert("pesan",$data);
	}
	
	public function lihat_alumni()
	{
		$id=$this->input->get('tahun');
		if ($id==''){
				$cari=$this->input->get('cari');
				if ($cari==''){
				$w='';
				}else{
				$w=$cari;
				}
				$this->db->select('id_peserta,nama,no_sertifikasi,tahun_lulus_ser');
				$this->db->from('peserta');
				$this->db->join("alumni", "alumni.id_alumni=peserta.id_peserta",'inner');
				$this->db->like('nama',$w);
				$this->db->order_by("nama"); 
				$params=$this->db->get();
				return $params->result();
		}else{
		$x=$id;
		$this->db->select('id_peserta,nama,no_sertifikasi,tahun_lulus_ser');
		$this->db->from('peserta');
		$this->db->join("alumni", "alumni.id_alumni=peserta.id_peserta",'inner');
		$this->db->where('tahun_lulus_ser',$x);
		$this->db->order_by("nama"); 
		$params=$this->db->get();
		return $params->result();
		}		

	}
	
	public function lihat_jenis_sertifikasi()
	{
		$this->db->select('*');
		$this->db->from('jenis_sertifikasi');
		$this->db->where('sh_jenis_sertifikasi','1');
		$params=$this->db->get();
		return $params->result();         
        
	}
	
	public function lihat_isi_home()
	{
		$this->db->select('*');
		$this->db->from('home');
		$params=$this->db->get();
		return $params->result();
	}
}