<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ko_cko extends CI_Controller {
    function __construct()
    {
        parent::__construct();
	$this->load->helper(array('url','form','tanggal'));
	$this->load->library(array('form_validation','pagination'));
	$this->load->database();
	$this->load->library('session');
	$this->load->model('model_skripsi');
    }
    
    function _view( $template = '', $param = '')
	{
        $this->load->view('ko_cko/header',$param);
        $this->load->view($template, $param);
        $this->load->view('ko_cko/footer');
    }
    
	public function index()
	{
		if($this->session->userdata('login'))
        {
            $session = $this->session->userdata('login');
		     $this->_view('ko_cko/home');
        }else{
            redirect('cek_session','refresh');   
        }
	}
	
	public function data_ilmu()
	{
		if($this->session->userdata('login'))
        {
			$ambil = $this->session->userdata('login');
								foreach ($ambil as $stat) {
									$id = $ambil['id'];
									}

            $session = $this->session->userdata('login');
			$params['data']=$this->model_skripsi->data_ilmu($id);
		    $this->_view('ko_cko/data_ilmu',$params);
        }else{
            redirect('cek_session','refresh');   
        }
	}
	
	public function input_ilmu()
	{
		if($this->session->userdata('login'))
        {
            $session = $this->session->userdata('login');
		     $this->_view('ko_cko/input_ilmu');
        }else{
            redirect('cek_session','refresh');   
        }
	}
	
	public function masuk_ilmu()
	{	
		if($this->session->userdata('login'))
        {
		$judul_data=$this->input->post('judul');
		$isi=$this->input->post('isi');
		$masa='1';
		$tgl_update=date('Y-m-d');
		$sh=$this->input->post('id_sh');
		$status=$this->input->post('validasi');
        $config['upload_path'] = './asset/gambar_upload/'; //lokasi folder yang akan digunakan untuk menyimpan file
		$config['allowed_types'] = 'gif|jpg|JPG|png|jpeg|JPEG'; //extension yang diperbolehkan untuk diupload
		$config['file_name'] = date('YmdHis').''.rand(1000,9999).'.jpg';
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		$judul=date('Y-m-d H:i:s');		
		if( !$this->upload->do_upload('file_upload'))
		{ 
		echo $this->upload->display_errors();
		}
		else{
		$data['upload_data']=$this->upload->data();

		$data = array(
			
			'id_masa'=>$masa,
			'judul_data_pertanian'=>$judul_data,
			'isi_data_pertanian'=>$isi,
			'last_update_ilmu'=>$tgl_update,
			'id_stake_holder'=>$sh,
			'status_validasi_ilmu_pertanian'=>$status,
			'tanggal_input_ilmu_pertanian'=>$tgl_update,
			'url_gambar_ilmu'		=> $data['upload_data']['file_name']
			
		);
		$this->model_skripsi->masuk_ilmu($data);
		redirect('ko_cko/data_ilmu');
		}
        }else{
            redirect('login','refresh');   
        }
	}
}