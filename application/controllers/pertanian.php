<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pertanian extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	$this->load->helper(array('url','form','tanggal'));
	$this->load->library(array('form_validation','pagination'));
	$this->load->database();
	$this->load->library('session');
	$this->load->model('petani_model');
    }
	
	function _view( $template = '', $param = '')
	{
        $this->load->view('petani/header');
        $this->load->view($template, $param);
        $this->load->view('petani/footer');
    }
    
	public function index()
	{
		$this->_view('petani/home');
	}
	
	public function ilmu($id=null)
	{
		
	$jml = $this->db->get('data_ilmu_pertanian');
	$config['base_url'] = base_url().'pertanian/ilmu';
	$config['total_rows'] = $jml->num_rows();
	$config['per_page'] = '2';
	$config['first_page'] = 'Awal';
	$config['last_page'] = 'Akhir';
	$config['next_page'] = '«';
	$config['prev_page'] = '»';
	$this->pagination->initialize($config);
	$data['halaman'] = $this->pagination->create_links();
	$data['param'] = $this->petani_model->ambil_ilmu($config['per_page'], $id);	
	$this->_view('petani/ilmu', $data);
	}
	
	public function ilmu_lengkap($id)
	{	
	$data['param'] = $this->petani_model->lihat_ilmu($id);	
	$this->_view('petani/lihat_ilmu', $data);
	}
	
	public function penyakit_lengkap($id)
	{	
	$data['param'] = $this->petani_model->lihat_penyakit($id);	
	$this->_view('petani/lihat_penyakit', $data);
	}
	
	public function penyakit()
	{	
	$data['param'] = $this->petani_model->penyakit();	
	$this->_view('petani/penyakit',$data);
	}
	
	public function lihat_gejala()
	{	
	$gejala = $this->petani_model->gej();	
	$this->_view('petani/gej',$data);
	}
	
	public function identifikasi()
	{	
	$batas=$this->input->get("batas");
	$jumlah_gejala_baru=0;
	$baru=1;
	$maks=0;
	for($i=1;$i<$batas;$i++){
	$id=$this->input->get("$i");
		if($id==''){	
		}else {
		$gejala_baru[$baru]=$id;
		$jumlah_gejala_baru=$jumlah_gejala_baru+1;
		$baru=$baru+1;
		}	
	}
	$gejala = $this->petani_model->gej();
	foreach($gejala as $baris)
	{
		$nilai=0;
		$pembagi=0;
		$gejala_lama=$baris->gejala_penyakit;
		$gejala_lama_pecah=explode(",",$gejala_lama);
		$jumlah_array=count($gejala_lama_pecah);
		$jum_array=$jumlah_array-1;
		for($x=0;$x<$jum_array;$x++){
		$gejala_banding=explode("_",$gejala_lama_pecah[$x]);
		for($y=1;$y<=$jumlah_gejala_baru;$y++){
			if($gejala_baru[$y]==$gejala_banding[0]){
			$nilai=$nilai+(1*$gejala_banding[1]);
			}
		}
		$pembagi=$pembagi+$gejala_banding[1];
		}
		$knn=$nilai/$pembagi;
		if ($knn>=$maks){
		$maks=$knn;
		$id_solusi=$baris->id_penyakit;
		}
	}	
	  $kemiripan=$maks*100;
	  $data['solusi'] = $this->petani_model->solusi($id_solusi);
	  $data['kemiripan'] = $kemiripan;
	  $this->_view('petani/gej', $data);
	}
	
	public function login()
	{
		$this->load->view('ko_cko/v_login');
	}
	
	 public function cari() {
        $data['data'] = $this->petani_model->cari_data();
		$data['penyakit'] = $this->petani_model->cari_penyakit();
        $this->_view('petani/cari', $data);
    }
}