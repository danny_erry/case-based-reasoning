<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Cek_login extends CI_Controller {
    public function index()
    {
        // load library form validasi , agar login kita lebih aman
        $this->load->library('form_validation');
 
        $this->load->helper('url'); // digunakan untuk fungsi redirect di bawah
 
        $this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean|callback_check_database');
 
        if($this->form_validation->run() == FALSE)
        {
 
            echo "<script>
                    alert('Gagal Login: Cek username dan password anda!');
                    history.go(-1);
                    </script>";
        }else
        {
			
			redirect('ko_cko','refresh');
			
            
 
        }
    }
 
    function check_database()
    {
        $this->load->library('session');
        //validasi kedua dengan cara mengecek database
        $username = $this->input->post('username');
        $password = $this->input->post('password');
 
        //query ke database dan memanggil model m_login
        $this->load->model('model_skripsi');
        $result = $this->model_skripsi->login($username,$password);
 
        //jika hasilnya ada pada maka masukan ke season field nama dan username dengan nama season : login
        if($result)
        {
            foreach($result as $row)
            {
                $sess_array = array(
					'id'=>$row->id_stake_holder,
                    'username' => $row->nama_pejabat,
					'hakakses'=>$row->id_hak_akses
                );
 
                $this->session->set_userdata('login', $sess_array);
            }
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}