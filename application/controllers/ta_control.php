<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ta_control extends CI_Controller {
    function __construct()
    {
        parent::__construct();
	$this->load->helper(array('url','form','tanggal'));
	$this->load->library(array('form_validation','pagination'));
	$this->load->database();
	$this->load->library('session');
	$this->load->model('ta_model');
    }
    
    function _view( $template = '', $param = '')
	{
        $this->load->view('admin/header',$param);
        $this->load->view($template, $param);
        $this->load->view('admin/footer');
    }
    
	public function index()
	{
		if($this->session->userdata('login'))
        {
            //mengambil nama dari session
            $session = $this->session->userdata('login');
            $params['nama'] = $session['nama'];
		    redirect('ta_control/lihat_pendaftar');   
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_sertifikasi()
	{	
		if($this->session->userdata('login'))
        {
            //mengambil nama dari session
            $session = $this->session->userdata('login');
            $params['data']=$this->ta_model->lihat_sertifikasi();
			$params['pen']='Jadwal Sertifikasi';
			$this->_view('admin/lihat_sertifikasi',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_input_sertifikasi()
	{
		if($this->session->userdata('login'))
        {
			$params['pen']='Input Jadwal Sertifikasi';
			$params['jenis']=$this->ta_model->lihat_jenis_sertifikasi();
            $this->_view('admin/input_sertifikasi',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function input_sertifikasi()
	{
		if($this->session->userdata('login'))
        {
         $this->ta_model->input_sertifikasi();
	redirect('ta_control/lihat_sertifikasi');
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function edit_sertifikasi($id='')
	{
		if($this->session->userdata('login'))
        {
         $params['data']=$this->ta_model->get_sertifikasi($id);
		 $params['pen']='Edit Jadwal Sertifikasi';
	$this->_view('admin/edit_sertifikasi',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function ubah_sertifikasi()
	{
		if($this->session->userdata('login'))
        {
        		$this->ta_model->edit_sertifikasi();
		redirect('ta_control/lihat_sertifikasi');
        }else{
            redirect('login','refresh');   
        }

	}
	
	public function hapus_sertifikasi($id)
	{
		if($this->session->userdata('login'))
        {
         $this->ta_model->delete_sertifikasi($id);
		redirect('ta_control/lihat_sertifikasi');
        }else{
            redirect('login','refresh');   
        }
	}
	

	
	public function do_upload()
	{	
		if($this->session->userdata('login'))
        {
         		$config['upload_path'] = './upload_image/'; //lokasi folder yang akan digunakan untuk menyimpan file
		$config['allowed_types'] = 'gif|jpg|JPG|png|jpeg|JPEG'; //extension yang diperbolehkan untuk diupload
		$config['file_name'] = date('YmdHis').''.rand(1000,9999).'.jpg';
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		if ($this->input->post('judul')=='') 
		{
			$judul=date('Y-m-d H:i:s');
		} else 
		{
			$judul=$this->input->post('judul');
		}
		$kategori=$this->input->post('kategori');
		$direk=$this->input->post('direk');
		$alamat=$this->input->post('alamat');
		$tahun=$this->input->post('tahun');
		if( !$this->upload->do_upload('file_upload'))
		{ 
		echo $this->upload->display_errors();
		}
		else{
		$data['upload_data']=$this->upload->data();

		$data = array(
			'judul_foto'	=>$judul,
			'url_foto'		=> $data['upload_data']['file_name'],
			'kategori_foto'	=>$kategori,
			'tahun_foto'		=>$tahun	,
			'sh_foto'			=>'1'
		);
		
	//	$config_resize=array(
		//'source_image'	=> $data['upload_data']['full_path'],
		//'new_image'		=>'./thumb/',
		//'maintain_ration'=>true,
		//'width'			=>160,
		//'height'		=>120
		//);
		//$this->load->library('image_lib',$config_resize);
		$this->ta_model->upload_gambar($data);
		redirect($direk);
		}
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function upload_slider()
	{	
		if($this->session->userdata('login'))
        {
        $config['upload_path'] = './upload_image/'; //lokasi folder yang akan digunakan untuk menyimpan file
		$config['allowed_types'] = 'gif|jpg|JPG|png|jpeg|JPEG'; //extension yang diperbolehkan untuk diupload
		$config['file_name'] = date('YmdHis').''.rand(1000,9999).'.jpg';
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		if ($this->input->post('judul')=='') 
		{
			$judul=date('Y-m-d H:i:s');
		} else 
		{
			$judul=$this->input->post('judul');
		}
		$keterangan=$this->input->post('keterangan');
		if( !$this->upload->do_upload('file_upload'))
		{ 
		echo $this->upload->display_errors();
		}
		else{
		$data['upload_data']=$this->upload->data();

		$data = array(
			'nama_slider'	=>$judul,
			'keterangan_slider'		=> $keterangan,
			'url_slider'		=> $data['upload_data']['file_name'],
			'sh_slider'			=>'1'
		);
		
	//	$config_resize=array(
		//'source_image'	=> $data['upload_data']['full_path'],
		//'new_image'		=>'./thumb/',
		//'maintain_ration'=>true,
		//'width'			=>160,
		//'height'		=>120
		//);
		//$this->load->library('image_lib',$config_resize);
		$this->ta_model->upload_slider($data);
		redirect('ta_control/slider',$params);
		}
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function slider()
	{
		if($this->session->userdata('login'))
        {
        $params['data']=$this->ta_model->lihat_slider();
		$params['pen']='Slider';
		$this->_view('admin/slider',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function ujian()
	{
		if($this->session->userdata('login'))
        {
        $params['data']=$this->ta_model->lihat_ujian();
		$params['tah']=$this->ta_model->lihat_tahun();
		$params['alamat']='ujian';
		$params['direk']='ta_control/ujian';
		$params['pen']='Foto Ujian';
		$this->_view('admin/galery',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function bootcamp()
	{
		if($this->session->userdata('login'))
        {
         $params['data']=$this->ta_model->lihat_bootcamp();
		 $params['tah']=$this->ta_model->lihat_tahun();
		$params['alamat']='bootcamp';
		$params['pen']='Foto Bootcamp';
		$params['direk']='ta_control/bootcamp';
		$this->_view('admin/galery',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function umum()
	{
		if($this->session->userdata('login'))
        {
        $params['data']=$this->ta_model->lihat_umum();
		$params['tah']=$this->ta_model->lihat_tahun();
		$params['alamat']='umum';
		$params['direk']='ta_control/umum';
		$params['pen']='Foto Umum';
		$this->_view('admin/galery',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function edit_foto($id='')
	{
		if($this->session->userdata('login'))
        {
         $params['data']=$this->ta_model->get_foto($id);
		$this->_view('admin/edit_foto',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function edit_slider($id='')
	{
		if($this->session->userdata('login'))
        {
         $params['data']=$this->ta_model->get_slider($id);
		$this->_view('admin/edit_slider',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function ubah_foto()
	{
		if($this->session->userdata('login'))
        {
         $this->ta_model->edit_foto();
			redirect('ta_control/ujian');
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function ubah_slider()
	{
		if($this->session->userdata('login'))
        {
         $this->ta_model->edit_slider();
			redirect('ta_control/slider');
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function hapus_foto($id)
	{
		if($this->session->userdata('login'))
        {
            $this->ta_model->delete_foto($id);
			redirect('ta_control/ujian');
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function hapus_slider($id)
	{
		if($this->session->userdata('login'))
        {
            $this->ta_model->delete_slider($id);
			redirect('ta_control/slider');
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_pendaftar()
	{
		if($this->session->userdata('login'))
        {
        $params['data']=$this->ta_model->lihat_pendaftar();	
		if($params['data'])
        {
		$params['tahun']=$this->ta_model->lihat_tahun_lulus();
		$params['ser']=$this->ta_model->lihat_jenis_sertifikasi();
		$params['pen']='Pendaftar Yang Belum Memiliki No. Sertifikasi';
         $this->_view('admin/lihat_pendaftar',$params);   
        }
        else
        {
            redirect('ta_control/lihat_semua_pendaftar');   
        }			
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_pendaftar_pisah($id)
	{
		if($this->session->userdata('login'))
        {
        $params['data']=$this->ta_model->lihat_pendaftar_pisah($id);	
		if($params['data'])
        {
		$params['tahun']=$this->ta_model->lihat_tahun_lulus();
		$params['ser']=$this->ta_model->lihat_jenis_sertifikasi();
		$params['pen']='Pendaftar Yang Belum Memiliki No. Sertifikasi';
         $this->_view('admin/lihat_pendaftar',$params);   
        }
        else
        {
            redirect('ta_control/lihat_pendaftar');   
        }			
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_semua_pendaftar()
	{
		if($this->session->userdata('login'))
        {
            $params['data']=$this->ta_model->lihat_semua_pendaftar();
			$params['ser']=$this->ta_model->lihat_jenis_sertifikasi();
			$params['tahun']=$this->ta_model->lihat_tahun_lulus();
			$params['pen']='Pendaftar Yang Sudah Memiliki No. Sertifikasi';
			$this->_view('admin/lihat_pendaftar_lengkap',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_semua_pendaftar_pisah($id)
	{
		if($this->session->userdata('login'))
        {
            $params['data']=$this->ta_model->lihat_semua_pendaftar_pisah($id);
			$params['ser']=$this->ta_model->lihat_jenis_sertifikasi();
			$params['tahun']=$this->ta_model->lihat_tahun_lulus();
			$params['pen']='Pendaftar Yang Sudah Memiliki No. Sertifikasi';
			$this->_view('admin/lihat_pendaftar_lengkap',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function profile_pendaftar($id)
	{
		if($this->session->userdata('login'))
        {
			$params['data']=$this->ta_model->profile_pendaftar($id);
			$params['pen']='Profile Pendaftar';
			$this->_view('admin/profile_pendaftar',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function no_sertifikasi()
	{
		if($this->session->userdata('login'))
        {
         $this->ta_model->no_sertifikasi($id);
		redirect('ta_control/lihat_pendaftar');
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_pesan()
	{
		if($this->session->userdata('login'))
        {
            $params['data']=$this->ta_model->lihat_pesan();
			$params['pen']='Pesan';
			$this->_view('admin/pesan',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function baca_pesan($id)
	{
		if($this->session->userdata('login'))
        {
            $this->ta_model->baca_pesan($id);
			redirect('ta_control/lihat_pesan');
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_profile_admin()
	{
		if($this->session->userdata('login'))
        {
            $params['data']=$this->ta_model->lihat_admin();
			$params['pen']='Admin';
			$this->_view('admin/profile',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function ubah_pas($id)
	{
		if($this->session->userdata('login'))
        {
            $params['data']=$this->ta_model->ubah_pas($id);
			$this->load->view('admin/ubah_pas',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function ganti_pas()
	{
		if($this->session->userdata('login'))
        {
			$nama=$this->input->post('nama');
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			$password=$this->input->post('password');
			$asli=$this->input->post('asli');
			$newpassword=$this->input->post('newpassword');
			$newpassworda=$this->input->post('newpassworda');
			if($asli==''){
			$data = array(
			'nama'	=>$nama,
			'username'		=> $username
		);
		$this->ta_model->ganti_pas($data,$username);
		redirect('login/logout');
		}else{
		if($asli==$password)
		{
		if($newpassword==$newpassworda){
		$data = array(
			'nama'	=>$nama,
			'username'		=> $username,
			'password' =>$newpassword);
		$this->ta_model->ganti_pas($data,$username);
		redirect('login/logout');
		}
		}
		}
		
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function masuk_alumni()
	{
		if($this->session->userdata('login'))
        {
		$batas=$this->input->get("batas");
            
			for($i=1;$i<=$batas;$i++){
			$params['data']=$this->ta_model->masuk_alumni($i);
			}
			//redirect('ta_control/lihat_pendaftar');
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_jenis_sertifikasi()
	{
		if($this->session->userdata('login'))
        {
            $params['data']=$this->ta_model->lihat_jenis_sertifikasi();
			$params['pen']='Sertifikasi Yang Ada';
			$this->_view('admin/lihat_jenis_sertifikasi',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_input_jenis_sertifikasi()
	{
		if($this->session->userdata('login'))
        {
			$params['pen']='Input Sertifikasi';
            $this->_view('admin/input_jenis_sertifikasi',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function input_jenis_sertifikasi()
	{
		if($this->session->userdata('login'))
        {
         $this->ta_model->input_jenis_sertifikasi();
	redirect('ta_control/lihat_jenis_sertifikasi');
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function edit_jenis_sertifikasi($id='')
	{
		if($this->session->userdata('login'))
        {
         $params['data']=$this->ta_model->get_jenis_sertifikasi($id);
		 $params['pen']='Edit Sertifikasi';
	$this->_view('admin/edit_jenis_sertifikasi',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function ubah_jenis_sertifikasi()
	{
		if($this->session->userdata('login'))
        {
        		$this->ta_model->edit_jenis_sertifikasi();
		redirect('ta_control/lihat_jenis_sertifikasi');
        }else{
            redirect('login','refresh');   
        }

	}
	
	public function hapus_jenis_sertifikasi($id)
	{
		if($this->session->userdata('login'))
        {
         $this->ta_model->delete_jenis_sertifikasi($id);
		redirect('ta_control/lihat_jenis_sertifikasi');
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function lihat_home()
	{
		if($this->session->userdata('login'))
        {
         $params['data']=$this->ta_model->lihat_home();
		 $params['pen']='Home';
		$this->_view('admin/home',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function edit_home($id='')
	{
		if($this->session->userdata('login'))
        {
         $params['data']=$this->ta_model->get_home($id);
		 $params['pen']='Edit Home';
		$this->_view('admin/edit_home',$params);
        }else{
            redirect('login','refresh');   
        }
	}
	
	public function ubah_home()
	{
		if($this->session->userdata('login'))
        {
        $this->ta_model->ubah_home();
		redirect('ta_control/lihat_home');
        }else{
            redirect('login','refresh');   
        }

	}
}