<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Siswa extends CI_Controller {
    function __construct()
    {
        parent::__construct();
	$this->load->helper(array('url','form','tanggal'));
	$this->load->library(array('form_validation','pagination'));
	$this->load->database();
	$this->load->model('model_user');
	$this->load->library('cezpdf');
$this->load->helper('pdf_helper');

    }
    
    function _view( $template = '', $param = '')
	{
        $this->load->view('user/header');
        $this->load->view($template, $param);
        $this->load->view('user/footer');
    }
    
	public function index()
	{
		redirect('siswa/home');
	}
	
	public function home()
	{		
		$params['data']=$this->model_user->lihat_slider();
		$params['isi']=$this->model_user->lihat_isi_home();
		$this->_view('user/home',$params);
	}

	public function lihat_galery()
	{
		$params['alamat']='Galery Foto';
		$params['tah']=$this->model_user->lihat_tahun();
		$params['data']=$this->model_user->lihat_galery();
		$this->_view('user/galery',$params);
	}
	
	public function lihat_ujian()
	{
		$params['alamat']='Foto Ujian';
		$params['tah']=$this->model_user->lihat_tahun();
		$params['data']=$this->model_user->lihat_ujian();
		$this->_view('user/galery',$params);
	}
	
	public function lihat_bootcamp()
	{
		$params['alamat']='Foto Boot Camp';
		$params['tah']=$this->model_user->lihat_tahun();
		$params['data']=$this->model_user->lihat_bootcamp();
		$this->_view('user/galery',$params);
	}
	
	public function lihat_umum()
	{
		$params['alamat']='Foto Umum';
		$params['tah']=$this->model_user->lihat_tahun();
		$params['data']=$this->model_user->lihat_umum();
		$this->_view('user/galery',$params);
	}
	
	public function lihat_sertifikasi($id)
	{
		$params['alamat']='Jadwal Sertifikasi';
		$params['data']=$this->model_user->lihat_sertifikasi($id);
		$this->_view('user/sertifikasi',$params);
	}
	
	public function input_pendaftaran($id = '')
	{
		$params['alamat']='Form Pendaftaran';
		$params['id'] = $id;
		$this->_view('user/daftar',$params);
	}
	
	public function input_pendaftar()
	{
		$id=$this->input->post("id");
		$this->form_validation->set_rules('jeniskelamin','jeniskelamin','required');
		$this->form_validation->set_rules('kodepos','kodepos','required|integer');
		$this->form_validation->set_rules('lulus','lulus','required|integer');
		
		if($this->form_validation->run()){
		$this->model_user->input_pendaftar();
		redirect('siswa/lihat_sertifikasi/'.$id);
		} else {
		echo "Ada data yang salah";
		}
	}
	
	public function contact()
	{
		$params['alamat']='Contact Us';
		$this->_view('user/contact',$params);
	}
	
	public function lihat_lokasi()
	{
		$params['alamat']='Lokasi Ujian';
		$this->_view('user/lokasi',$params);
	}
	
	public function kirimpesan()
	{
		$this->model_user->input_pesan();
		redirect('siswa/contact');
	}
	
	public function lihat_alumni()
	{
		$params['alamat']='Alumni';
		$params['tah']=$this->model_user->lihat_tahun_lulus();
		$params['data']=$this->model_user->lihat_alumni();
		$this->_view('user/alumni',$params);
	}
	
	function cetak()
	{
		$data['data']=$this->model_user->lihat_alumni();
		$title=array(
					'id_peserta'=>'id_peserta',
					'nama'=>'nama',
					'no_sertifikasi'=>'no_sertifikasi',
					'tahun_lulus_ser'=>'tahun_lulus_ser'
					);
		$this->cezpdf->ezTable($data['data']);
		$this->cezpdf->ezStream();
	}
}