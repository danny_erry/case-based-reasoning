<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kelompokwangi extends CI_Controller {
    function __construct()
    {
        parent::__construct();
	$this->load->helper(array('url','form'));
	$this->load->library(array('form_validation','pagination'));
	$this->load->database();
	$this->load->model('mahasiswa');
    }
    
    function _view( $template = '', $param = '')
	{
        $this->load->view('header');
        $this->load->view($template, $param);
        $this->load->view('footer');
    }
    
	public function index()
	{
		$this->_view('header');
	}
	
	public function lihat_mahasiswa()
	{
		$params['data']=$this->mahasiswa->lihat_mahasiswa();
		$this->_view('body',$params);
	}
	
	public function inputmahasiswa()
	{
	$this->_view('inputmahasiswa');
	}
	
	public function input_mahasiswa()
	{
	$this->mahasiswa->masuk_mahasiswa();
	redirect('kelompokwangi/lihat_mahasiswa');
	}
	
	public function edit_mahasiswa($id='')
	{
	$params['data']=$this->mahasiswa->get_nrp($id);
	$this->_view('editmahasiswa',$params);
	}
	
	public function ubahmahasiswa()
	{
		$this->mahasiswa->edit_mahasiswa();
		redirect('kelompokwangi/lihat_mahasiswa');
	}
	
	public function hapus_mahasiswa($id)
	{
		$this->mahasiswa->delete_mahasiswa($id);
		redirect('kelompokwangi/lihat_mahasiswa');
	}
}